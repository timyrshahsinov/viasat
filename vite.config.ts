import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import VueDevTools from 'vite-plugin-vue-devtools'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VueDevTools(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      '@section': fileURLToPath(new URL('./src/components/section', import.meta.url)),
      '@molecule': fileURLToPath(new URL('./src/components/molecule', import.meta.url)),
      '@atom': fileURLToPath(new URL('./src/components/atom', import.meta.url))
    }
  }
})
